## 試したこと

- https://www.insight-tec.com/tech-blog/20200929_gitlab/
    - この例を試したプロジェクト

- [公式](https://gitlab-docs.creationline.com/ee/ci/quick_start/#configuring-a-runner)
    - 次に試したやつ

## メモ

- リポジトリにプッシュすると、GitLabは.gitlab-ci.ymlファイルを探し、そのファイルの内容にしたがって_Runner_上でそのコミットのためのジョブを開始します。


Rubeだとこんな感じ
```yml
image: "ruby:2.5"

before_script:
  - apt-get update -qq && apt-get install -y -qq sqlite3 libsqlite3-dev nodejs
  - ruby -v
  - which ruby
  - gem install bundler --no-document
  - bundle install --jobs $(nproc)  "${FLAGS[@]}"

rspec:
  script:
    - bundle exec rspec

rubocop:
  script:
    - bundle exec rubocop
```